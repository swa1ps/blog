from django.db import models

class Author(models.Model):
  name = models.CharField(max_length=255)
  reg_date = models.DateField(auto_now=True)

  def __str__(self):
    return f'{self.name}, {self.reg_date}'

class Post(models.Model):
  name = models.CharField(max_length=255)
  text = models.TextField()
  is_paid = models.BooleanField(default=False)
  author = models.ForeignKey(Author, on_delete=models.CASCADE, null=True)

  def __str__(self):
    return self.name