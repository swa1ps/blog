from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import Post, Author

def index(request):
    posts = Post.objects.order_by('-is_paid')
    context = {
      'posts': posts
    }
    return render(request, 'index.html', context)


def index_api(request):
    posts = Post.objects.select_related('author').all()

    # authors = Author.objects.all()
    posts_list = []

    for post in posts:
      post_data = {
        'name': post.name,
        'text': post.text,
        'author': post.author.name
      }
      posts_list.append(post_data)

    data = {
      'posts': posts_list
    }

    return JsonResponse(data=data)

